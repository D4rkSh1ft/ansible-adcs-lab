# SSH Authentication

## Windows

Copy `sshd_config` and `administrators_authorized_keys`
```
scp administrators_authorized_keys sshd_config Administrator@<IP>:C:/ProgramData/ssh/
```

Restart `sshd`
```
ssh Administrator@<IP> "powershell -c restart-service sshd"
```

```Powershell
Set-Item -Path WSMan:\localhost\Service\Auth\Basic -Value $true
```

```
ssh Administrator@<IP> "powershell -c Set-Item -Path WSMan:\localhost\Service\Auth\Basic -Value 1"
ssh Administrator@1<IP> "powershell -c Set-Item -Path WSMan:\localhost\Service\Auth\CredSSP -Value 1"
ssh Administrator@<IP> "powershell -c Set-Item -Path WSMan:\localhost\Service\AllowUnencrypted -Value 1"
ssh Administrator@1<IP> "powershell -c Set-Item WSMan:\localhost\Client\TrustedHosts -Value * -Force"
```

```
$ pipx inject ansible pywinrm
```
